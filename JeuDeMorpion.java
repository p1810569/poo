public class JeuDeMorpion {

    private Plateau plateau;
    private Joueur joueurs[];
    private int courrant;

    public JeuDeMorpion(Joueur j0, Joueur j1) {
        plateau = new Plateau(3, 3);
        joueurs = new Joueur[2];

        joueurs[0] = j0;
        joueurs[1] = j1;

        courrant = 0;
        plateau.initialiser();
    }

    private Joueur joueurSuivant() {
        courrant = (courrant + 1) % joueurs.length;
        return joueurs[courrant];
    }

    public boolean coupPossible(Coup coup) {
        if (coup == null)
            return false;
        if (coup.y < plateau.longueur && coup.x < plateau.largeur)
            return plateau.etatPlateau[coup.y][coup.x] == 0;

        return false;
    }

    public boolean partieTerminee() {

        for (int i=0; i<plateau.longueur; ++i) {
            for (int j = 0; j < plateau.largeur; ++j) {
                if (plateau.etatPlateau[i][j] == 0)
                    return false;
            }
        }
        return true;
    }

    public Joueur jouerPartie() {
        Joueur retour = null;

        while (!partieTerminee()) {
            retour = joueurSuivant();
            Coup c = retour.getCoup(plateau);
            if (coupPossible(c)) {
                plateau.appliquerCoup(c, retour.getId());
                try {
                    Runtime.getRuntime().exec("cls");
                } catch (Exception e) {
                    System.out.println("failed to clear terminal");
                }
                System.out.println(plateau.toString());
            } else {
                System.err.println(" erreur ");
                System.exit(1);
            }
        }
        return retour;
    }
}
