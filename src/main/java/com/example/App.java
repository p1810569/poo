package com.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        int numbers = Integer.parseInt(args[0]);

        double sum = 0.0;
        for (int i=1; i<=numbers; ++i)
            sum += 1.0 / i;
        
        System.out.println(sum);
    }
}