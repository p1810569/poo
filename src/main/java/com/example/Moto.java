package com.example;

import javax.annotation.processing.RoundEnvironment;

public class Moto {
    
    private Roue roues[];
    private Phare phares[];
    private Moteur moteur;

    public Moto() {
        roues = new Roue[2];
        phares = new Phare[2];
        moteur = new Moteur();
    }
}
